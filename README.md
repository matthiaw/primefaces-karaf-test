# Primesfaces-Karaf-Example #

A simple example for primefaces in karaf.

### Usage ###

Compile with 
```
mvn clean install
```

and deploy in karaf with
```
feature:repo-add mvn:org.rogatio/primefaces-karaf-test-features/LATEST/xml; feature:install primefaces-karaf
```

Start in browser with 
```
http://localhost:8181/primekaraftest/
```